Support
###############################################################################

:slug: support
:modified: 2015-10-20 10:20

.. _Spice User Manual: spice-user-manual.html
.. _spice for newbies: spice-for-newbies.html
.. _Spice list archive: http://lists.freedesktop.org/archives/spice-devel/
.. _issues: https://gitlab.freedesktop.org/groups/spice/-/issues
.. _Features: features.html

Using Spice for the first time?
+++++++++++++++++++++++++++++++

`Spice user manual`_ provides all the information on how to get, build, install and use Spice. You can also read `Spice for newbies`_ in case you want to have a better understanding about Spice architecture and features.

Need help using Spice?
++++++++++++++++++++++

Many questions can be answered in the `Spice user manual`_ and the `Spice list archive`_. If you can't find the answer there, post your question to spice-devel@lists.freedesktop.org. Be sure to detail your issue so that we can offer the best response.

Have some feedback to share?
++++++++++++++++++++++++++++
Spice project would like to have your feedback - either negative or positive. Please post your feedback to spice-devel@lists.freedesktop.org.

Found a bug? Want to help and report?
+++++++++++++++++++++++++++++++++++++
#. Look at `issues`_ to see whether it is a known bug.

   - There is no need to open a new issue in case it is already known, but optionally you can add your comment to the existing issue.
   - In case you are not using the latest version of Spice, upgrade and see if it was resolved.

#. Create a new issue under the relevant project in Spice `issues`_ and follow these guidelines:

   - Write clear and detailed description of the bug.
   - Provide instructions on how to reproduce the bug.
   - Specify all relevant components and their versions.
   - Attach relevant logs.

     - Spice server sends its log messages to stdout, use G_MESSAGES_DEBUG=all for debug-level logging.
     - Spice client sends its log messages to stdout, use --spice-debug to enable debug level logging.

   - Provide the necessary information:

     - Crash - provide the crash message.
     - Client bug - specify the OS type and version.
     - Wrong rendering - attach a screen shot. (You will have to take client screen shot so make sure that the cursor is not on Spice window area while taking the screen shot).

Your help for tracking down the bug is very important, so please try to be as cooperative as possible and help us resolve the bug. 

Like to have a new feature?
+++++++++++++++++++++++++++
First search `issues`_ to see whether it was already requested. In addition, refer to the Features_ page. If it was not requested you can submit a new issue using a clear and detailed description.